﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    PhysNavAgent Agent;
    GameObject go;
    public float m_Knockback;
    void Update()
    {
        if(go==null)
            go = GameObject.Find("Agent");
        else
        {
            if (Agent == null)
                Agent = go.GetComponent<PhysNavAgent>();
        }
        if(Agent!=null)
        {
            if (Input.GetMouseButton(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000))
                {
                    Debug.DrawRay(hit.point, Vector3.up * 5, Color.blue, 1f);
                    Agent.SetTargetDirection(hit.point);
                }
            }
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000))
                {
                    Agent.m_rigidbody.AddExplosionForce(m_Knockback, hit.point, 10, 1, ForceMode.Impulse);
                }
            }
        }
    }
}
