﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PhysNavAgent : MonoBehaviour
{
    public Rigidbody m_rigidbody { get; private set; }
    NavMeshAgent m_navAgent;

    public Vector3 MovementDirection;
    Vector3 worldDeltaPosition;

    public float
        turnSpeed = 5,
        moveDrag,
        turnDragQuad = 5,
        moveDragStop = 5,
        moveDragQuad = 3;

    float accel = 120; //Convert to navagents accel
    float speedToUse = 10; // convert to navagent speed

    NavMeshPath path;
    
    private void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_navAgent = GetComponent<NavMeshAgent>();
        m_navAgent.updatePosition = false;
        accel = m_navAgent.acceleration;
        speedToUse = m_navAgent.speed;
        path = new NavMeshPath();
    }

    private void Update()
    {
        accel = m_navAgent.acceleration;
        speedToUse = m_navAgent.speed;
        CheckOnGround();
        if(physAgentOnGround)
        {
            HorizontalMovement();
            ApplyDragHorizontal();
        }
        DebugMovement();
    }

    private void FixedUpdate()
    {
        if (worldDeltaPosition.magnitude > m_navAgent.radius)
            m_navAgent.nextPosition = transform.position + ((m_navAgent.radius) * worldDeltaPosition);
    }

    float lastInputMagnitudeH;
    Vector3 currentGroundNormal = Vector3.up;
    Vector3 moveDragVelocity;

    void HorizontalMovement()
    {
        Vector3 accel =  speedToUse * MovementDirection;
        accel = Vector3.ProjectOnPlane(accel, currentGroundNormal).normalized * accel.magnitude;
        m_rigidbody.AddForce(accel, ForceMode.Acceleration);
    }

    void ApplyDragHorizontal()
    {
        lastInputMagnitudeH = Mathf.Lerp(lastInputMagnitudeH, MovementDirection.magnitude, .3f);

        //horizontal quadratic drag
        Vector3 hVel = Vector3.ProjectOnPlane(m_rigidbody.velocity, currentGroundNormal);

        float dragAmount = (hVel.sqrMagnitude * moveDragQuad * .1f) * moveDrag;

        //apply extra (inverse quadratic) drag when stopping
        if (MovementDirection.magnitude < lastInputMagnitudeH * .9f)
        {
            dragAmount += Mathf.Clamp(Mathf.Pow(hVel.magnitude, .5f) * moveDragStop * Mathf.Clamp01(1 - MovementDirection.magnitude), 0, hVel.magnitude * 50 * Mathf.Abs(MovementDirection.magnitude));
        }

        moveDragVelocity = -hVel.normalized * dragAmount;
        m_rigidbody.AddForce(moveDragVelocity, ForceMode.Acceleration);
    }

    public bool physAgentOnGround;
    public Transform distanceCheckTransform;
    
    RaycastHit groundHit;
    void CheckOnGround()
    {
        physAgentOnGround =  Physics.Raycast(distanceCheckTransform.position, Vector3.down, out groundHit, .75f);
    }


    public bool navAgentOnPath;
    public void DebugMovement()
    {
        navAgentOnPath = m_navAgent.isOnNavMesh;
        Vector3 StartPos = transform.position + m_rigidbody.velocity.normalized;
        Vector3 Direction = m_rigidbody.velocity;

        Debug.DrawRay(StartPos, Direction, Color.green);
        Debug.DrawRay(StartPos + Direction, -moveDragVelocity, Color.red);
        Debug.DrawRay(m_navAgent.nextPosition, Vector3.up * 6, Color.magenta);
    }

    Vector3 targetPosition { get; set; }
    Coroutine movement = null;
    public void SetTargetDirection(Vector3 _Position)
    {
        targetPosition = _Position;
        m_navAgent.CalculatePath(_Position, path);
        m_navAgent.destination = _Position;
        if (movement == null)
            movement = StartCoroutine(Move());        
    }

  

    IEnumerator Move()
    {
        m_navAgent.CalculatePath(targetPosition, path);

        while (navAgentOnPath)
        {

            if (physAgentOnGround)
            {
                m_navAgent.isStopped = false;
                worldDeltaPosition = m_navAgent.nextPosition - transform.position;
                m_navAgent.CalculatePath(targetPosition, path);               
                MovementDirection = (m_navAgent.nextPosition - transform.position).normalized;
            }
            else
            {
                worldDeltaPosition = Vector3.zero;
                m_navAgent.isStopped = true ;
            }
            yield return new WaitForEndOfFrame();
            if (m_navAgent.remainingDistance < 1)
            {
                m_navAgent.nextPosition = transform.position;
                MovementDirection = Vector3.zero;
                worldDeltaPosition = Vector3.zero;
                m_navAgent.isStopped = true;
                break;
            }
        }
        movement = null;
        yield return null;
    }
}
